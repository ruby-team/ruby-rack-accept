ruby-rack-accept (0.4.5-4) unstable; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on ruby-rack.
    + ruby-rack-accept: Drop versioned constraint on ruby-rack in Depends.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

  [ Antonio Terceiro ]
  * Rack::Accept::Response: drop usage of deprecated method.
    This fixes this package to work with ruby-rack 3.1

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 10 Mar 2025 15:52:54 -0300

ruby-rack-accept (0.4.5-3) unstable; urgency=medium

  * Team upload
  * Upload to unstable

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 15 Jul 2015 16:12:05 -0300

ruby-rack-accept (0.4.5-2) experimental; urgency=medium

  * Team upload
  * Update packaging with a new dh-make-ruby run

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 15 Jul 2015 15:28:27 -0300

ruby-rack-accept (0.4.5-2~exp1) experimental; urgency=medium

  * Team upload.
  * Target experimental and build with ruby 2.2.
  * Add ruby-test-unit to Build-Depends for ruby2.2.
  * Update Vcs-Browser to cgit URL and HTTPS.
  * Bump Standards-Version to 3.9.6 (no further changes).

 -- Sebastien Badia <seb@sebian.fr>  Mon, 20 Apr 2015 00:05:20 +0200

ruby-rack-accept (0.4.5-1) unstable; urgency=low

  * Initial release (Closes: #715820)

 -- Nitesh A Jain <niteshjain92@gmail.com>  Wed, 10 Jul 2013 23:51:29 +0530
